var chai = require('chai'), chaiHttp = require('chai-http');
chai.use(chaiHttp);
var assert = chai.assert;
var should = chai.should();
var expect = chai.expect;



const url = "http://localhost:3001"

describe('Test Servicio Cliente obtener una orden', function(){
    it('should generate a order',(done)=>{
        
        chai.request(url)
        .get('/orden')
        .end(function(err, res){
            expect(isNaN(res.body.idorden)).to.be.false
            expect(res).to.have.status(200);
            done();
        });
    });
})

describe('Test Servicio Cliente obtener estado de una orden', function(){
    it('should get state from order',(done)=>{
        
        chai.request(url)
        .get('/estado/10')
        .end(function(err, res){
            expect(isNaN(res.text)).to.be.true
            expect(res).to.have.status(200);
            done();
        });
    });
})