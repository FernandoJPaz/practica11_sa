const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const request = require('request');
var fecha = new Date();
var rp = require('request-promise');

//Index 
app.get('/', function (req, res) {
    res.send('Saludos desde Servidor Cliente');

});

//Cliente envia orden
app.get('/orden', function(req,res){
    var orden = Math.floor(Math.random() * (100-1)+1)
    var descripcion = "Se creo una orden con identificador: "+orden
    //Escribo en el archivo log 
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })
    //Envio Orden al orquestador
    request.post('http://localhost:3010/postorden',{

        json:{
            'idorden':orden
        }
    })
    var fecha = new Date();
    res.json({'idorden':orden , 'descripcion':descripcion, 'fecha': fecha })    
})

//Cliente envia consulta de su orden
app.get('/estado/:orden',body_parser,function(req,res){

    var orden = req.params.orden
    var descripcion = "Cliente desea saber el estado de la orden: "+orden
    
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })

    
    rp.get('http://localhost:3010/estado/' + orden)
    .then(function(response){
        
        res.json(response)

    })
    .catch(function (err) {
        console.log(err)
    });
    
});

//Cliente envia consulta de su orden al repartidor
app.get('/repartidor/estado/:orden',body_parser,function(req,res){

    var orden = req.params.orden
    var descripcion = "Cliente desea saber el estado de la orden: "+orden
    
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })

    
    rp.get('http://localhost:3010/repartidor/estado/' + orden)
    .then(function(response){
        
        res.json(response)

    })
    .catch(function (err) {
        console.log(err)
    });
    
});

// Puerto 3001
app.listen(3001, () => {
 console.log("El servidor Cliente está inicializado en el puerto 3001");
});