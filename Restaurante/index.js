const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const request = require('request');
var rp = require('request-promise');

//Index
app.get('/', function (req, res) {
    res.send('Saludos desde Servidor Restaurante');

});

//Restaurante recibe orden del cliente
app.post('/postorden',body_parser, function(req,res){
    var order = req.body.idorden
    var descripcion = "Restaurante recibe una orden: "+order
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })
    descripcion = "Restaurante prepara orden: "+order
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })
    res.send("OK")
});

//Estado de la orden
app.get('/estado/:orden',body_parser, function(req,res){

    //Restaurante recibe una consulta sobre el estado de una orden 
    var orden = req.params.orden
    var descripcion = "Restaurante recibe una consulta de orden: "+orden +" para saber el estado de la misma"
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })

    var aLetras = new Array('a', 'b', 'c');
    var cLetra = aLetras[Math.floor(Math.random()*aLetras.length)]; 

    if(cLetra == 'a'){
        descripcion = 'Orden: '+orden+' Estado: '+' Preparandose'
    }else if (cLetra == 'b'){
        descripcion = 'Orden: '+orden+' Estado: '+' Enviandose'
    }else {
        descripcion = 'Orden: '+orden+' Estado: '+' No Recibida y Cancelada'
    }
    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })
    //res.json(JSON.stringify({'state': cLetra , 'descripcion':descripcion}))
    res.send(descripcion)
});

//Asignacion de orden y cliente para x Repartidor 
app.post('/Entrega',body_parser,function(req,res){
    var orden = req.body.idorden
    var descripcion = "La orden: "+orden+" esta lista para repartir, se le asignara un repartidor"

    request.post('http://localhost:3010/log',{
        json:{
            'descripcion':descripcion
        }
    })
    
    rp.post('http://localhost:3010/orden/Repartidor',{
        json:{
            'idorden': orden
        }
    })
    .then(function(response){
        
        res.json(response)

    })
    .catch(function (err) {
        console.log(err)
    });


});



//Servidor run en puerto 3000
app.listen(3000, () => {
 console.log("El servidor Restaurante está inicializado en el puerto 3000");
});

